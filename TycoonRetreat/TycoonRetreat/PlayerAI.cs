﻿using MonopolyTest1.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonopolyTest1.Contracts.Commands;

namespace TycoonRetreat
{
    public class PlayerAI : IPlayerAI
    {
        public int Delay { get; set; } = 500;
        public string LevelPassword { get; set; } = "saturn";
        public string CompanyName { get; set; } = "Bram en Robin Test";

        public IPlayerCommand GetAIAction(ReadonlySimulationState state)
        {
            if (state.BankBalance < 500000)
            {
                if (state.BankBalance >= 100000)
                {
                    if (state.LastMonthRecurringDelta*12 >= 400000)
                    {
                        return new DoNothingCommand();
                    }
                    var coord = state.Map.Tiles.FirstOrDefault(t => t.Type == TileType.House && !t.IsOwned)?.Coordinate;
                    if (coord.HasValue)
                        return new BuyCommand(coord.Value);
                }
                return new DoNothingCommand();
            }
            var factory = state.Map.Tiles.FirstOrDefault(t => t.Type == TileType.Factory && !t.IsOwned)?.Coordinate;
            if (factory.HasValue)
            {
                return new BuyCommand(factory.Value);
            }
            return new DoNothingCommand();
        }
    }
}
